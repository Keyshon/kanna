const User = require('../model/User');
const Retailer = require('../model/Retailer');

/**
 * Retailer core class handling all retailer account operations.
 */
class RetailerCore {
  /**
   * Creates retailer account if possible and returns object { field: String }.
   * @param {Object} data Retailer object { field: String } to be inserted.
   * @param {String} userId User id: String to be linked with created customer account.
   */
  static async create(data, userId) {
    // Test if current user has no retailer account
    const currentUser = await User.findById(userId);
    if (currentUser.retailerAccount !== null) {
      const error = new Error('Retailer account already exists.');
      error.status = 400;
      return error;
    }

    // Save new retailer account and then update user
    const retailerAccount = new Retailer({ field: data.field });
    let savedRetailerAccount;
    try {
      savedRetailerAccount = await retailerAccount.save();
      await currentUser.updateOne({
        retailerAccount: savedRetailerAccount._id,
      });
    } catch (err) {
      savedRetailerAccount.deleteOne();
      const error = new Error(err);
      error.status = 400;
      return error;
    }

    return savedRetailerAccount;
  }
}

module.exports = RetailerCore;
