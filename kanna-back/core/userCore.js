const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const TokenGenerator = require('../util/TokenGenerator');
const User = require('../model/User');
const {
  loginValidation,
  registerValidation,
  identificatorEmailValidation,
} = require('../validation');

/**
 * User core class handling all user operations.
 */
class UserCore {
  /**
   * Saves to database and returns instance of user {name: String, email: String, password: String, birthDate: Date}.
   * @param {Object} data user object {id: String, name: String, email: String, birthDate: Date}.
   */
  static async create(data) {
    // Validate user against schema
    const { error: validationError } = registerValidation(data);
    if (validationError) {
      const error = new Error(validationError.details[0].message);
      error.status = 400;
      return error;
    }

    // Prevent duplicates
    const userExists = await User.findOne({
      $or: [{ name: data.name }, { email: data.email }],
    });
    if (userExists) {
      const error = new Error('User already exists.');
      error.status = 400;
      return error;
    }

    // Generate salt and hash
    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(data.password, salt);
    // Create user instance
    const user = new User({
      name: data.name,
      email: data.email,
      password: passwordHash,
      birthDate: data.birthDate,
      customerAccount: null,
      retailerAccount: null,
    });

    // Save user to database
    try {
      const savedUser = await user.save();
      return {
        id: savedUser._id,
        name: savedUser.name,
        email: savedUser.email,
        birthDate: savedUser.birthDate,
      };
    } catch (err) {
      const error = new Error(err);
      error.status = 400;
      return error;
    }
  }

  /**
   * Authenticates user and returns token: String.
   * @param {Object} data Login object containing {identificator: String, password: String}.
   */
  static async login(data) {
    // Validate login data against schema
    const { error: validationError } = loginValidation(data);
    if (validationError) {
      const error = new Error(validationError.details[0].message);
      error.status = 400;
      return error;
    }

    // Validate user exists
    const identificator = data.identificator;
    const isEmail = identificatorEmailValidation({
      identificator,
    }).error
      ? false
      : true;
    const user = isEmail
      ? await User.findOne({ email: identificator })
      : await User.findOne({ name: identificator });
    if (!user) {
      const error = new Error('Username or Password not matching.');
      error.status = 400;
      return error;
    }

    // Validate password is correct but show the same error to not reveal that user exists
    const validPassword = await bcrypt.compare(data.password, user.password);
    if (!validPassword) {
      const error = new Error('Username or Password not matching.');
      error.status = 400;
      return error;
    }

    // Generate token
    const tokenGenerator = new TokenGenerator(
      process.env.TOKEN_SECRET,
      process.env.TOKEN_SECRET,
      { expiresIn: '1h' }
    );
    const token = tokenGenerator.sign(
      { _id: user._id },
      {
        issuer: `${process.env.TOKEN_ISSUER}`,
        jwtid: '1',
      }
    );
    // Return fresh json web token
    return token;
  }

  /**
   * Returns refreshed token: String.
   * @param {String} token Current token.
   */
  static refresh(token) {
    const tokenGenerator = new TokenGenerator(
      process.env.TOKEN_SECRET,
      process.env.TOKEN_SECRET,
      { expiresIn: '1h' }
    );

    const jwtid =
      Number.parseInt(jwt.decode(token, { complete: true }).payload.jti) + 1;

    const refreshToken = tokenGenerator.refresh(token, {
      verify: {
        issuer: `${process.env.TOKEN_ISSUER}`,
      },
      jwtid: String(jwtid),
    });

    return refreshToken;
  }

  /**
   * Returns user id: String by supplied token: String.
   * @param {String} token Token to decode.
   */
  static getActiveUserId(token) {
    const payload = jwt.decode(token);

    if ('_id' in payload) {
      return payload._id;
    } else {
      const error = new Error('No user id defined.');
      error.status = 400;
      return error;
    }
  }

  /**
   * Deletes a user.
   * @param {String} id Id of user to be deleted.
   */
  static async delete(id) {
    const foundUser = await User.findById(id);
    if (foundUser === null) {
      const error = new Error('User not found.');
      error.status = 404;
      return error;
    }

    try {
      await User.findByIdAndDelete(id);
    } catch (err) {
      const error = new Error(err);
      error.status = 400;
      return error;
    }
  }
}

module.exports = UserCore;
