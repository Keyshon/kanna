const User = require('../model/User');
const Customer = require('../model/Customer');

/**
 * Customer core class handling all customer account operations.
 */
class CustomerCore {
  /**
   * Creates customer account if possible and returns object { field: String }.
   * @param {Object} data Customer object { field: String } to be inserted.
   * @param {String} userId User id: String to be linked with created customer account.
   */
  static async create(data, userId) {
    // Test if current user has no customer account
    const currentUser = await User.findById(userId);
    if (currentUser.customerAccount !== null) {
      const error = new Error('Customer account already exists.');
      error.status = 400;
      return error;
    }

    // Save new customer account and then update user
    const customerAccount = new Customer({ field: data.field });
    let savedCustomerAccount;
    try {
      savedCustomerAccount = await customerAccount.save();
      await currentUser.updateOne({
        customerAccount: savedCustomerAccount._id,
      });
    } catch (err) {
      savedCustomerAccount.deleteOne();
      const error = new Error(err);
      error.status = 400;
      return error;
    }

    return savedCustomerAccount;
  }
}

module.exports = CustomerCore;
