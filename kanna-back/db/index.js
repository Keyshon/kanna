const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

connect = () => {
  return new Promise((resolve, reject) => {
    if (process.env.NODE_ENV === 'test') {
      const Mockgoose = require('mockgoose').Mockgoose;
      const mockgoose = new Mockgoose(mongoose);

      mockgoose
        .prepareStorage()
        .then(() => {
          mongoose.connect(
            process.env.DB_CONNECT,
            {
              useNewUrlParser: true,
              useUnifiedTopology: true,
            },
            (err, res) => {
              if (err) {
                console.log(err);
                return reject(err);
              }

              console.log('Mocked MongoDB connection established.');
              resolve();
            }
          );
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      mongoose.connect(
        process.env.DB_CONNECT,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        },
        (err, res) => {
          if (err) {
            console.log(err);
            return reject(err);
          }

          console.log('MongoDB connection established.');
          resolve();
        }
      );
    }
  });
};

close = () => {
  return mongoose.disconnect();
};

module.exports = { connect, close };
