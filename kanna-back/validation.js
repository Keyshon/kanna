const Joi = require('@hapi/joi');

const loginValidation = (data) => {
  const schema = Joi.object({
    identificator: Joi.string().min(5).max(255).required(),
    password: Joi.string().min(6).max(1024).required(),
  });
  return schema.validate(data);
};
const registerValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string().min(5).max(255).required(),
    email: Joi.string().min(8).max(255).required().email(),
    password: Joi.string().min(6).max(1024).required(),
    birthDate: Joi.date(),
  });
  return schema.validate(data);
};
const identificatorEmailValidation = (data) => {
  const schema = Joi.object({
    identificator: Joi.string().min(8).max(255).email(),
  });
  return schema.validate(data);
};

module.exports = {
  loginValidation: loginValidation,
  registerValidation: registerValidation,
  identificatorEmailValidation: identificatorEmailValidation,
};
