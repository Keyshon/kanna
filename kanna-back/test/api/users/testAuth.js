process.env.NODE_ENV = 'test';

const expect = require('chai').expect;
const request = require('supertest');

const app = require('../../../app');
const db = require('../../../db');

describe('Authentication API', () => {
  const url = '/api/users';

  before((done) => {
    db.connect()
      .then(() => done())
      .catch((err) => done(err));
  });

  after((done) => {
    db.close()
      .then(() => done())
      .catch((err) => done(err));
  });

  describe(`Sending POST ${url}`, () => {
    it('should register a full user', (done) => {
      const user = {
        name: 'Automated Test',
        email: 'automated.test@example.com',
        password: process.env.TEST_USER_PASSWORD,
        birthDate: '1997-06-19T00:00:00.000+00:00',
      };

      request(app)
        .post(url)
        .send(user)
        .then((res) => {
          const body = res.body;
          expect(res.status).to.be.equal(201);
          expect(body).to.contain.property('id');
          expect(body).to.contain.property('name');
          expect(body).to.contain.property('email');
          expect(body).to.contain.property('birthDate');
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should register a user without optional data', (done) => {
      const user = {
        name: 'Optionalautomated Test',
        email: 'optionalautomated.test@example.com',
        password: process.env.TEST_USER_PASSWORD,
      };

      request(app)
        .post(url)
        .send(user)
        .then((res) => {
          const body = res.body;
          expect(res.status).to.be.equal(201);
          expect(body).to.contain.property('id');
          expect(body).to.contain.property('name');
          expect(body).to.contain.property('email');
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should not create a duplicating user by name', (done) => {
      const user = {
        name: 'Automated Test',
        email: 'newautomated.test@example.com',
        password: process.env.TEST_USER_PASSWORD,
        birthDate: '1997-06-19T00:00:00.000+00:00',
      };

      request(app)
        .post(url)
        .send(user)
        .then((res) => {
          expect(res.status).to.be.equal(400);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should not create a duplicating user by email', (done) => {
      const user = {
        name: 'Newautomated Test',
        email: 'automated.test@example.com',
        password: process.env.TEST_USER_PASSWORD,
        birthDate: '1997-06-19T00:00:00.000+00:00',
      };

      request(app)
        .post(url)
        .send(user)
        .then((res) => {
          expect(res.status).to.be.equal(400);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should not create a user in invalid format', (done) => {
      const user = {
        name: 'Noemailautomated Test',
        email: 'incorrect@email',
        password: process.env.TEST_USER_PASSWORD,
        birthDate: '1997-06-19T00:00:00.000+00:00',
      };

      request(app)
        .post(url)
        .send(user)
        .then((res) => {
          expect(res.status).to.be.equal(400);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe(`Sending POST ${url}/login`, () => {
    it('should authenticate existing user by name', (done) => {
      const authDetails = {
        identificator: 'Automated Test',
        password: process.env.TEST_USER_PASSWORD,
      };

      request(app)
        .post(`${url}/login`)
        .send(authDetails)
        .then((res) => {
          expect(res.status).to.be.equal(200);
          expect(res.header).to.contain.property('auth-token');
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
    it('should authenticate existing user by email', (done) => {
      const authDetails = {
        identificator: 'automated.test@example.com',
        password: process.env.TEST_USER_PASSWORD,
      };

      request(app)
        .post(`${url}/login`)
        .send(authDetails)
        .then((res) => {
          expect(res.status).to.be.equal(200);
          expect(res.header).to.contain.property('auth-token');
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
    it('should not authenticate existing user by incorrect name', (done) => {
      const authDetails = {
        identificator: 'Wrong Identificator',
        password: process.env.TEST_USER_PASSWORD,
      };

      request(app)
        .post(`${url}/login`)
        .send(authDetails)
        .then((res) => {
          expect(res.status).to.be.equal(400);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
    it('should not authenticate existing user by incorrect email', (done) => {
      const authDetails = {
        identificator: 'wrong@identifica.tor',
        password: process.env.TEST_USER_PASSWORD,
      };

      request(app)
        .post(`${url}/login`)
        .send(authDetails)
        .then((res) => {
          expect(res.status).to.be.equal(400);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
    it('should not authenticate existing user by incorrect password', (done) => {
      const authDetails = {
        identificator: 'Automated Test',
        password: 'IncorrectPassword1',
      };

      request(app)
        .post(`${url}/login`)
        .send(authDetails)
        .then((res) => {
          expect(res.status).to.be.equal(400);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe(`Sending POST ${url}/refresh`, () => {
    let token, refreshedToken;

    before((done) => {
      const authDetails = {
        identificator: 'Automated Test',
        password: process.env.TEST_USER_PASSWORD,
      };

      request(app)
        .post(`${url}/login`)
        .send(authDetails)
        .then((res) => {
          token = res.header['auth-token'];
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should refresh valid token', (done) => {
      request(app)
        .post(`${url}/refresh`)
        .set('auth-token', token)
        .then((res) => {
          expect(res.status).to.be.equal(200);
          expect(res.header).to.have.property('auth-token');
          refreshedToken = res.header['auth-token'];
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should refresh refreshed token', (done) => {
      request(app)
        .post(`${url}/refresh`)
        .set('auth-token', refreshedToken)
        .then((res) => {
          expect(res.status).to.be.equal(200);
          expect(res.header).to.have.property('auth-token');
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should not refresh empty token', (done) => {
      request(app)
        .post(`${url}/refresh`)
        .set('auth-token', '')
        .then((res) => {
          expect(res.status).to.be.equal(401);
          expect(res.header).to.not.have.property('auth-token');
          done();
        })
        .catch((err) => {
          done(err);
        });
    });

    it('should not refresh invalid token', (done) => {
      request(app)
        .post(`${url}/refresh`)
        .set('auth-token', 'wrongtoken')
        .then((res) => {
          expect(res.status).to.be.equal(400);
          expect(res.header).to.not.have.property('auth-token');
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
});
