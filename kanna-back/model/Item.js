const mongoose = require('mongoose');
const PriceQuote = require('./PriceQuote');
const Retailer = require('./Retailer');

/**
 * Contains an item description.
 */
const itemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 3,
    max: 255,
  },
  description: {
    type: String,
    max: 4196,
  },
  stockAmount: {
    type: Number,
    required: true,
    min: 0,
    max: 9999,
  },
  priceQuotes: {
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: PriceQuote.modelName,
      },
    ],
  },
  retailer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Retailer.modelName,
  },
});

module.exports = mongoose.model('Item', itemSchema);
