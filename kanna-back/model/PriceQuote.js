const mongoose = require('mongoose');
const Unit = require('./../enum/units');

/**
 * Contains an amount-price correspondance.
 */
const priceQuoteSchema = new mongoose.Schema({
  price: {
    type: Number,
    required: true,
    min: 0,
    max: 999999,
  },
  amount: {
    type: Number,
    required: true,
    min: 0,
    max: 999999,
  },
  unit: {
    type: String,
    enum: [Unit.GRAMS, Unit.ITEMS],
    default: Unit.GRAMS,
  },
});

module.exports = mongoose.model('PriceQuote', priceQuoteSchema);
