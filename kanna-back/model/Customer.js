const mongoose = require('mongoose');
const Order = require('./Order');

const customerSchema = new mongoose.Schema({
  field: {
    type: String,
  },
  orders: [{ type: mongoose.Schema.Types.ObjectId, ref: Order.modelName }],
});

module.exports = mongoose.model('Customer', customerSchema);
