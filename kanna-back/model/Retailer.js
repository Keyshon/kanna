const mongoose = require('mongoose');
const Order = require('./Order');
const Item = require('./Item');

const retailerSchema = new mongoose.Schema({
  field: {
    type: String,
  },
  orders: [{ type: mongoose.Schema.Types.ObjectId, ref: Order.modelName }],
  items: [{ type: mongoose.Schema.Types.ObjectId, ref: Item.modelName }],
});

module.exports = mongoose.model('Retailer', retailerSchema);
