const mongoose = require('mongoose');
// TODO: Circular dependance
const Order = require('./Order');
const Customer = require('./Customer');
const Retailer = require('./Retailer');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 5,
    max: 255,
  },
  email: {
    type: String,
    required: true,
    min: 8,
    max: 255,
  },
  password: {
    type: String,
    required: true,
    min: 6,
    max: 1024,
  },
  birthDate: {
    type: Date,
  },
  customerAccount: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Customer.modelName,
  },
  retailerAccount: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Retailer.modelName,
  },
});

// Hooks to delete any related instances before deleting the object itself
userSchema.pre('remove', async function (next) {
  try {
    await Customer.findOneAndDelete(this.customerAccount);
    await Retailer.findOneAndDelete(this.retailerAccount);
    next();
  } catch (err) {
    next(err);
  }
});
userSchema.pre('findOneAndDelete', async function (next, doc) {
  try {
    await ClieCustomernt.findOneAndDelete(this.customerAccount);
    await Retailer.findOneAndDelete(this.retailerAccount);
    next();
  } catch (err) {
    next(err);
  }
});

module.exports = mongoose.model('User', userSchema);
