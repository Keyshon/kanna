const mongoose = require('mongoose');
const Item = require('./Item');
const Status = require('./../enum/orderStatuses');
const Customer = require('./Customer');
const Retailer = require('./Retailer');

const orderSchema = new mongoose.Schema({
  items: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: Item.modelName,
    },
  ],
  status: {
    type: String,
    enum: [
      Status.QUOTE,
      Status.ACCEPTED,
      Status.CANCELLED,
      Status.DELIVERED,
      Status.FINALIZED,
      Status.SHIPPED,
    ],
    default: Status.QUOTE,
  },
  retailer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Retailer.modelName,
  },
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Customer.modelName,
  },
});

module.exports = mongoose.model('Order', orderSchema);
