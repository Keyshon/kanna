const express = require('express');
const app = express();

// Middlewares
app.use(express.json());

// Routes
const userRoute = require('./routes/users');
const customerRoute = require('./routes/customers');
const retailerRoute = require('./routes/retailers');

// Middleware Routes
app.use('/api/customers', customerRoute);
app.use('/api/retailers', retailerRoute);
app.use('/api/users', userRoute);

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use((req, res, next) => {
  const error = new Error('Route not found.');
  error.status = 404;
  next(error);
});

app.use((err, req, res, next) => {
  return res.status(err.status || 500).json({ error: err.message });
});

module.exports = app;
