const units = {
  // Grams
  GRAMS: 'grams',
  // Items / units
  ITEMS: 'items',
};

module.exports = units;
