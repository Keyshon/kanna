const orederStatuses = {
  // Order just created
  QUOTE: 'Quote',
  // Order is cancelled by any reason
  CANCELLED: 'Cancelled',
  // Order is accepted (confirmed to be processed) by retailer
  ACCEPTED: 'Accepted',
  // Order is shipped
  SHIPPED: 'Shipped',
  // Order is delivered
  DELIVERED: 'Delivered',
  // Order is finalized (processed)
  FINALIZED: 'Finalized',
};

module.exports = orederStatuses;
