const router = require('express').Router();
const verify = require('../middleware/verifyToken');
const CustomerCore = require('../core/customerCore');
const UserCore = require('../core/userCore');

router.post('/', verify, async (req, res, next) => {
  // Get authentication token and active user id
  const token = req.header('auth-token');
  const currentId = UserCore.getActiveUserId(token);

  // Create and get customer account
  const result = await CustomerCore.create(req.body, currentId);

  // Return final outcome
  if (result instanceof Error) {
    next(result);
  } else {
    return res.status(200).json(result);
  }
});

module.exports = router;
