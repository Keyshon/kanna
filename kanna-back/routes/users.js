const router = require('express').Router();
const verify = require('../middleware/verifyToken');
const UserCore = require('../core/userCore');

const authTokenHeader = 'auth-token';

router.post('/', async (req, res, next) => {
  const result = await UserCore.create(req.body);

  if (result instanceof Error) {
    next(result);
  } else {
    return res.status(201).json(result);
  }
});

router.post('/login', async (req, res, next) => {
  const result = await UserCore.login(req.body);

  if (result instanceof Error) {
    next(result);
  } else {
    return res
      .header(authTokenHeader, result)
      .status(200)
      .json({ message: 'Logged in.' });
  }
});

router.post('/refresh', verify, (req, res) => {
  const token = req.header(authTokenHeader);

  const result = UserCore.refresh(token);

  return res
    .header(authTokenHeader, result)
    .status(200)
    .json({ message: 'Token refreshed.' });
});

router.get('/getCurrentUser', verify, (req, res, next) => {
  const token = req.header(authTokenHeader);

  const result = UserCore.getActiveUserId(token);

  if (result instanceof Error) {
    next(result);
  } else {
    return res.status(200).json(result);
  }
});

router.delete('/:userId', verify, async (req, res, next) => {
  const result = await UserCore.delete(req.params.userId);

  if (result instanceof Error) {
    next(result);
  } else {
    return res.status(204).json();
  }
});

module.exports = router;
