const app = require('./app');
const db = require('./db');

const PORT = process.env.PORT || 5000;

db.connect().then(() => {
  app.listen(PORT, () => {
    console.log(`Kanna Backend started on ${PORT}...`);
  });
});
